<!DOCTYPE html>
<html>
<head>
  <title>Daniel Cefram Ramirez | Software Developer</title>
  <meta charset="utf-8">
  <meta property="og:title" content="danielcefram.com" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://danielcefram.com/" />
  <meta property="og:image" content="https://danielcefram.com/assets/images/logo.png" />
  <meta property="og:description" content="Vanity website of a software developer" />
  <meta name="description" content="Vanity website of a software developer">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href="https://fonts.googleapis.com/css?family=Raleway:200,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="assets/css/reset.css" />
  <link rel="stylesheet" type="text/css" href="assets/css/style.css" />

  <link rel="apple-touch-icon" sizes="57x57" href="assets/images/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="assets/images/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="assets/images/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="assets/images/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="assets/images/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="assets/images/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="assets/images/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon-16x16.png">
  <link rel="manifest" href="assets/images/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="assets/images/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
</head>
<body>
  <div class="main-container">
    <div class="logo-container">
      <div class="logo"></div>
      <div class="firstname -orange">
        <span class="-large">D</span>aniel
      </div>
      <div class="surname -gray">
        <span class="-large">C</span>efram
      </div>
    </div>
    <div class="desc-container">
      <p class="first-line">Web Developer</p>
      <p class="second-line">
        <a href="https://splitmedialabs.com" target="_blank">SplitmediaLabs</a>
      </p>
      <p class="third-line">I appreciate</p>
      <p class="fourth-line">Clean Design</p>
      <p class="fifth-line" id="default">
        <a href="https://daeram.com">Write</a> . <a href="https://github.com/dcefram" target="_blank">Code</a> . <a href="#" id="draw">Draw</a>
      </p>
      <p id="hidden-text" hidden="true">
        No Time Yet :(
      </p>
    </div>
  </div>

  <div class="socials">
    <a href="https://twitter.com/dcefram" target="_blank"><ion-icon name="logo-twitter"></ion-icon></a>
    <a href="https://www.linkedin.com/in/daniel-cefram-ramirez" target="_blank"><ion-icon name="logo-linkedin"></ion-icon></a>
    <a href="mailto:me@danielcefram.com" target="_blank"><ion-icon name="mail"></ion-icon></a>
  </div>

  <script src="https://unpkg.com/ionicons@4.4.4/dist/ionicons.js"></script>

  <script>
    const draw = document.getElementById('draw');
    const def = document.getElementById('default');
    const hidden = document.getElementById('hidden-text');

    draw.addEventListener('click', event => {
      hidden.removeAttribute('hidden');
      def.setAttribute('hidden', true);
      event.preventDefault();
    });

    hidden.addEventListener('click', () => {
      def.removeAttribute('hidden');
      hidden.setAttribute('hidden', true);
    })
  </script>
</body>
</html>
